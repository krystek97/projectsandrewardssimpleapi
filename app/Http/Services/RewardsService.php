<?php

namespace App\Http\Services;

use App\ApiResponse;
use App\Reward;

class RewardsService
{
    /**
     * @var Reward
     */
    private $reward;
    /**
     * @var ApiResponse
     */
    private $apiResponse;

    public function __construct(Reward $reward, ApiResponse $apiResponse)
    {
        $this->reward = $reward;
        $this->apiResponse = $apiResponse;
    }

    public function storeReward($dataToSaveReward)
    {
        $this->reward->projectId = $dataToSaveReward['projectId'];
        $this->reward->amount = $dataToSaveReward['amount'];
        $this->reward->name = $dataToSaveReward['name'];
        $this->reward->description = $dataToSaveReward['description'];
        $this->reward->save();
        return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['200'], $this->apiResponse->responseTypes['success'], $this->apiResponse->responseMessages[4]);
    }
}
