<?php

namespace App\Http\Services;

use App\ApiResponse;
use App\Project;

class ProjectsService
{
    /**
     * @var ApiResponse
     */
    private $apiResponse;
    /**
     * @var Project
     */
    private $project;
    /**
     * @var RewardsService
     */
    private $rewardService;

    public function __construct(ApiResponse $apiResponse, Project $project, RewardsService $rewardsService)
    {
        $this->apiResponse = $apiResponse;
        $this->project = $project;
        $this->rewardService = $rewardsService;
    }

    public function findProjectByStatus($status)
    {
        $projects = $this->project->with('rewards')->where('status', $status)->get();
        return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['200'], $this->apiResponse->responseTypes['success'], $projects);
    }

    public function storeProject($dataToSave)
    {
        $this->project['name'] = $dataToSave['name'];
        $this->project['description'] = $dataToSave['description'];
        $this->project['status'] = $dataToSave['status'] ?? 'started';
        $this->project->save();
        return $this->apiResponse->getResponseMessage($this->apiResponse->responseTypes['success'], $this->apiResponse->statusCodes['200'], $this->apiResponse->responseMessages[1]);
    }

    public function findProjectById($idProject)
    {
        $project = $this->project->with("rewards")->find($idProject);
        return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['200'], $this->apiResponse->responseTypes['success'], $project);
    }

    public function updateProject($dataToUpdate, $project)
    {
        $project->name = $dataToUpdate['name'];
        $project->description = $dataToUpdate['description'];
        $project->status = $dataToUpdate['status'] ?? 'started';
        $project->save();
        return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['200'], $this->apiResponse->responseTypes['success'], $this->apiResponse->responseMessages[2]);
    }

    public function deleteProject($id)
    {
        $project = $this->project->find($id);
        $project->delete();
        return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['200'], $this->apiResponse->responseTypes['success'], $this->apiResponse->responseMessages[3]);
    }
}
