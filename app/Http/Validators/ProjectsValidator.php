<?php

namespace App\Http\Validators;

use App\Http\Components\JsonResponseComponent;
use App\Http\Services\ProjectsService;
use App\Http\Services\RewardsService;
use App\Project;
use App\Reward;
use App\ApiResponse;
use Illuminate\Support\Facades\Validator;

class ProjectsValidator{
    /**
     * @var ProjectsService
     */
    private $projectService;
    /**
     * @var RewardsService
     */
    private $rewardService;
    /**
     * @var Project
     */
    private $project;
    /**
     * @var ApiResponse
     */
    private $apiResponse;
    /**
     * @var Reward
     */
    private $reward;
    /**
     * @var RewardsValidator
     */
    private $rewardValidator;

    public function __construct(ApiResponse $apiResponse, Project $project, RewardsService $rewardsService, ProjectsService $projectsService, Reward $reward, RewardsValidator $rewardsValidator){
        $this->apiResponse = $apiResponse;
        $this->project = $project;
        $this->rewardService = $rewardsService;
        $this->projectService = $projectsService;
        $this->reward = $reward;
        $this->rewardValidator = $rewardsValidator;
    }

    public function idProjectExist($idForFind, $requestData, $messageForNotFoundId, $operationType)
    {
        $project = $this->project->find($idForFind);
        if ($project === null) {
            return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['404'], $this->apiResponse->responseTypes['error'], $messageForNotFoundId);
        } else if ($operationType === $this->project->projectOperation['update']) {
            return $this->projectService->updateProject($requestData, $project);
        } else if ($operationType === $this->project->projectOperation['delete']) {
            return $this->projectService->deleteProject($idForFind);
        } else if ($operationType === $this->reward->rewardOperation['store']) {
            return $this->rewardValidator->validateDataToStoreReward($requestData);
        }else if($operationType === $this->project->projectOperation['findProjectById']){
            return $this->projectService->findProjectById($idForFind);
        }
    }

    public function idProjectIsValid($idForValidate, $operationType, $messageForBadId, $messageForNotFoundId ,$requestData = '')
    {
        $rulesForId = ['id' => 'integer'];
        $dataToValidate = ['id' => $idForValidate];
        $idIsValidate = Validator::make($dataToValidate, $rulesForId);
        if ($idIsValidate->fails()) {
            return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['400'], $this->apiResponse->responseTypes['error'], $messageForBadId);
        } else {
            return $this->idProjectExist($idForValidate, $requestData, $messageForNotFoundId, $operationType);
        }
    }

    public function statusIsValid($status, $typeOperation, $messageForInvalidStatus, $requestData = '')
    {
        if (!in_array($status, (['started', 'draft', 'finished']))) {
            return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['400'], $this->apiResponse->responseTypes['error'], $messageForInvalidStatus);
        } else if($typeOperation === $this->project->projectOperation['findByStatus']){
            return $this->projectService->findProjectByStatus($status);
        }
    }

    public function validateDataToStoreAndUpdateProject($requestData, $typeOperation, $messageForBadRequest, $idProject = '')
    {
        $validator = Validator::make($requestData, $this->project->rules);
        if ($validator->fails()) {
            return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['405'], $this->apiResponse->responseTypes['error'], $messageForBadRequest);
        } else if ($typeOperation === $this->project->projectOperation['store']) {
            return $this->projectService->storeProject($requestData);
        } else if ($typeOperation === $this->project->projectOperation['update']) {
            return $this->idProjectIsValid($idProject, $typeOperation, $this->apiResponse->responseMessages[7], $this->apiResponse->responseMessages[5] ,$requestData);
        }
    }
}
