<?php

namespace App\Http\Validators;

use App\ApiResponse;
use App\Http\Services\RewardsService;
use App\Reward;
use Illuminate\Support\Facades\Validator;

class RewardsValidator{
    /**
     * @var RewardsService
     */
    private $rewardsService;
    /**
     * @var Reward
     */
    private $reward;
    /**
     * @var ApiResponse
     */
    private $apiResponse;

    public function __construct(RewardsService $rewardsService, Reward $reward, ApiResponse $apiResponse){
        $this->rewardsService = $rewardsService;
        $this->reward = $reward;
        $this->apiResponse = $apiResponse;
    }

    public function validateDataToStoreReward($dataToSaveReward)
    {
        $validatorDataToStoreReward = Validator::make((array)$dataToSaveReward, $this->reward->rules);
        if ($validatorDataToStoreReward->fails()) {
            return $this->apiResponse->getResponseMessage($this->apiResponse->statusCodes['405'], $this->apiResponse->responseTypes['error'], $this->apiResponse->responseMessages[6]);
        } else {
            return $this->rewardsService->storeReward($dataToSaveReward);
        }
    }
}
