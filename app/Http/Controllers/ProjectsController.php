<?php

namespace App\Http\Controllers;

use App\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Validators\ProjectsValidator;
use App\Project;

class ProjectsController extends Controller
{
    /**
     * @var ProjectsValidator
     */
    private $projectValidator;
    /**
     * @var Project
     */
    private $project;
    /**
     * @var ApiResponse
     */
    private $apiResponse;

    public function __construct(ProjectsValidator $projectValidator, Project $project, ApiResponse $apiResponse)
    {
        $this->projectValidator = $projectValidator;
        $this->project = $project;
        $this->apiResponse = new ApiResponse();
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->projectValidator->validateDataToStoreAndUpdateProject($request->all(), $this->project->projectOperation['store'], $this->apiResponse->responseMessages[6]);
        } else if ($request->isMethod('put')) {
            return $this->projectValidator->validateDataToStoreAndUpdateProject($request->all(), $this->project->projectOperation['update'],  $this->apiResponse->responseMessages[8], $request->id);
        }
    }

    public function singleProjectOperation($id, Request $request)
    {
        if ($request->isMethod('delete')) {
            return $this->projectValidator->idProjectIsValid($id, $this->project->projectOperation['delete'],  $this->apiResponse->responseMessages[7],  $this->apiResponse->responseMessages[5]);
        }else if($request->isMethod("get")){
            return $this->projectValidator->idProjectIsValid($id, $this->project->projectOperation['findProjectById'],  $this->apiResponse->responseMessages[7],  $this->apiResponse->responseMessages[6]);
        }else if($request->isMethod("post")){
            return $this->projectValidator->validateDataToStoreAndUpdateProject($request->all(), $this->project->projectOperation['update'],  $this->apiResponse->responseMessages[8], $id);
        }
    }

    public function findByStatus(Request $request)
    {
        if($request->isMethod('get')) {
            return $this->projectValidator->statusIsValid($request->query('status'), $this->project->projectOperation['findByStatus'],  $this->apiResponse->responseMessages[9]);
        }
    }
}
