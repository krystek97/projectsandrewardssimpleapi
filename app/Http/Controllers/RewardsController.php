<?php

namespace App\Http\Controllers;

use App\ApiResponse;
use App\Http\Validators\ProjectsValidator;
use App\Reward;
use Illuminate\Http\Request;

class RewardsController extends Controller
{
    /**
     * @var ProjectsValidator
     */
    private $projectValidator;
    /**
     * @var Reward
     */
    private $reward;

    public function __construct(ProjectsValidator  $projectValidator, Reward $reward, ApiResponse $apiResponse)
    {
        $this->projectValidator = $projectValidator;
        $this->reward = $reward;
        $this->apiResponse = new ApiResponse();
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->projectValidator->idProjectIsValid($request->projectId, $this->reward->rewardOperation['store'],  $this->apiResponse->responseMessages[6], $this->apiResponse->responseMessages[6] ,$request->all());
        }
    }
}
