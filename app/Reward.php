<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'amount', 'created_at', 'updated_at'
    ];

    public $rewardOperation = [
      'store' => 'storeReward'
    ];

    public $rules = [
        'name' => 'required',
        'description' => 'required',
        'amount' => 'required|numeric',
    ];
}
