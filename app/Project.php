<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $rules = [
        'name' => 'required',
        'description' => 'required',
        'status' => 'in:started,finished,draft'
    ];

    public $projectOperation = [
      'store' => 'storeProject',
      'update' => 'updateProject',
      'delete' => 'deleteProject',
      'findByStatus' => 'findByStatus',
      'findProjectById' => 'findProjectById'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'status', 'created_at', 'updated_at'
    ];

    public function rewards()
    {
        return $this->hasMany('App\Reward', 'projectId', 'id');
    }
}
