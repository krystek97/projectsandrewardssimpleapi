<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiResponse extends Model
{
    public $statusCodes = [
        '200' => 200,
        '405' => 405,
        '400' => 400,
        '404' => 404
    ];

    public $responseMessages = [
       1 => 'You have added a new project',
       2 => 'You have updated the project',
       3 => 'You have deleted the project',
       4 => 'You have added a new reward',
       5 => 'Project not found',
       6 => 'Invalid Input',
       7 => 'Invalid ID supplied',
       8 => 'Validation Exception',
       9 => 'Invalid status value'
    ] ;

    public $responseTypes = [
      'success' => 'success',
      'error' => 'error',
      'info' => 'info'
    ];

    public function getResponseMessage($code, $type, $message)
    {
        return response()->json(['code' => $code, 'type' => $type, 'message' => $message]);
    }
}
