<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Project;

class ProjectsControllerTest extends TestCase
{
    public function __construct(){
        parent::__construct();
        $this->project = new Project();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddProjectBadData()
    {
        $countProjectsBeforeOperation =  $this->project->count();
        $response = $this->json('POST', '/project', ['name' => 'Sally']);
        $countProjectsAfterOperation =  $this->project->count();
        $response->assertJson([
            'code' => 405,
        ]);
        $this->assertEquals($countProjectsBeforeOperation, $countProjectsAfterOperation);
    }

    public function testFindByIdGoodData(){
        $response = $this->call('GET', '/project/30');
        $response->assertJson([
            'code' => 200,
        ]);
    }

    public function testFindByIdBadData(){
        $response = $this->call('GET', '/project/45');
        $response->assertJson([
            'code' => 404
        ]);
    }

    public function testFindByStatusGoodData(){
        $response = $this->call('GET', '/project/findByStatus', ['status' => 'started']);
        $response->assertJson([
            'code' => 200,
        ]);
    }

    public function testFindByStatusBadData(){
        $response = $this->call('GET', '/project/findByStatus', ['status' => 'drafts']);
        $response->assertJson([
            'code' => 400,
        ]);
    }

    public function testAddProjectGoodData(){
        $countProjectsBeforeOperation =  $this->project->count();
        $response = $this->json('POST', '/project', ['name' => 'Project', 'description' => 'description project']);
        $countProjectsAfterOperation =  $this->project->count();
        $response->assertJson([
            'code' => 200,
        ]);
        $this->assertEquals($countProjectsBeforeOperation + 1, $countProjectsAfterOperation);
    }

    public function testUpdateProjectBadDataInvalidId(){
        $response = $this->json('PUT', '/project', ['name' => 'Project', 'description' => 'description project', 'id' => 'wer']);
        $response->assertJson([
            'code' => 400
        ]);
    }

    public function testUpdateProjectBadDataValidationException(){
        $response = $this->json('PUT', '/project', ['name' => 'Project', 'description' => '', 'id' => 'wer']);
        $response->assertJson([
            'code' => 405
        ]);
    }

    public function testUpdateProjectGoodData(){
        $idProject = $this->project->first();
        $response = $this->json('PUT', '/project', ['name' => 'Project', 'description' => 'Jakiś tam opis', 'id' => $idProject->id]);
        $response->assertJson([
            'code' => 200
        ]);
    }

    public function testDeleteProjectBadDataInvalidId(){
        $response = $this->json('DELETE', '/project/sdfsdf');
        $response->assertJson([
            'code' => 400
        ]);
    }

    public function testDeleteProjectGoodData(){
        $idProject = $this->project->first();
        $response = $this->json('DELETE', '/project/'.$idProject->id);
        $response->assertJson([
            'code' => 200
        ]);
    }
}
