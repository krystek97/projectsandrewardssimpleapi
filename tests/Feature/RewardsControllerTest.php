<?php

namespace Tests\Feature;

use App\Project;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Reward;

class RewardsControllerTest extends TestCase{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->reward = new Reward();
        $this->project = new Project();
    }

//    public function testAddRewardGoodData(){
//        $countRewardsBeforeOperation = $this->reward->count();
//        $response = $this->json('POST', '/reward', ['amount' => '12.22', 'name' => 'Jakaś nagroda', 'description' => 'Jakiś tam opis', 'projectId' => 9]);
//        $countRewardsAfterOperation =  $this->reward->count();
//        $response->assertJson([
//            'code' => 200,
//        ]);
//        $this->assertEquals($countRewardsBeforeOperation + 1, $countRewardsAfterOperation);
//    }

//    public function testAddRewardBadData(){
//        $countRewardsBeforeOperation = $this->reward->count();
//        $response = $this->json('POST', '/reward', ['amount' => 'sdfs', 'name' => 'jjk', 'description' => 'Jakiś tam opis', 'projectId' => 19]);
//        $countRewardsAfterOperation =  $this->reward->count();
//        $response->assertJson([
//            'code' => 405,
//        ]);
//        $this->assertEquals($countRewardsBeforeOperation, $countRewardsAfterOperation);
//    }
}
