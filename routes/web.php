<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('project')->group(function () {
    Route::match(['post', 'put'], '', 'ProjectsController@index');
    Route::get('/findByStatus', 'ProjectsController@findByStatus');
    Route::match(['delete', 'get', 'post'], '/{id}', 'ProjectsController@singleProjectOperation');
});

Route::prefix('reward')->group(function () {
    Route::post('', 'RewardsController@index');
});
