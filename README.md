<h1>Project Information - Simple Api</h1>

## About Project
Simple REST API with projects and theirs rewards

## Installation
1.From your projects root folder to install project run:<br/>
<code>docker-compose build</code><br/>
2.From your projects root folder to run project run:<br/>
<code>docker-compose up -d</code><br/>
3.From your projects root to import database to db container:<br/>
<code>1.docker exec -it projectsandrewardssimpleapi_db_1 bash -l</code><br/>
<code>2.mysql -u root -p projects_app_database < ./docker-entrypoint-initdb.d/projects_app_database.sql</code>

## Endpoints
<h5>1.POST /reward</h5>
<img alt="example request 1" src="/public/readme-images/example-request1.png"/><br/>
<img alt="example request 2" src="/public/readme-images/example-request2.png"/>
<h5>2.PUT /project</h5>
<img alt="example request 3" src="/public/readme-images/example-request3.png"/><br/>
<img alt="example request 4" src="/public/readme-images/example-request4.png"/><br/>
<img alt="example request 5" src="/public/readme-images/example-request5.png"/><br/>
<img alt="example request 6" src="/public/readme-images/example-request6.png"/>
<h5>3.GET /project/findByStatus</h5>
<img alt="example request 7" src="/public/readme-images/example-request7.png"/><br/>
<img alt="example request 8" src="/public/readme-images/example-request8.png"/>
<h5>4.DELETE /project/{projectId}</h5>
<img alt="example request 9" src="/public/readme-images/example-request9.png"/><br/>
<img alt="example request 10" src="/public/readme-images/example-request10.png"/><br/>
<img alt="example request 11" src="/public/readme-images/example-request11.png"/>
<h5>5.POST /project</h5>
<img alt="example request 12" src="/public/readme-images/example-request12.png"/><br/>
<img alt="example request 13" src="/public/readme-images/example-request13.png"/>
<h5>6.GET /project/{projectId}</h5>
<img alt="example request 14" src="/public/readme-images/example-request14.png"/><br/>
<img alt="example request 15" src="/public/readme-images/example-request15.png"/>
<h5>7.POST /project/{projectId}</h5>
<img alt="example request 16" src="/public/readme-images/example-request16.png"/><br/>
<img alt="example request 17" src="/public/readme-images/example-request17.png"/>
