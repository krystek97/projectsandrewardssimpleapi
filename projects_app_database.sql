-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 29 Paź 2020, 08:41
-- Wersja serwera: 10.4.14-MariaDB
-- Wersja PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projects_app_database`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('started','finished','draft') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(24, 'Project', 'description project', 'started', '2020-10-22 08:58:02', '2020-10-22 08:58:02'),
(25, 'Project', 'description project', 'started', '2020-10-23 05:52:14', '2020-10-23 05:52:14'),
(26, 'Project', 'description project', 'started', '2020-10-23 05:53:40', '2020-10-23 05:53:40'),
(27, 'Project', 'description project', 'started', '2020-10-23 05:54:48', '2020-10-23 05:54:48'),
(28, 'Project', 'description project', 'started', '2020-10-23 05:55:16', '2020-10-23 05:55:16'),
(29, 'Project', 'description project', 'started', '2020-10-23 05:58:01', '2020-10-23 05:58:01'),
(31, 'Project', 'description project', 'started', '2020-10-23 05:58:55', '2020-10-23 05:58:55'),
(32, 'Project', 'description project', 'started', '2020-10-27 08:53:02', '2020-10-27 08:53:02'),
(33, 'Jakiś projekt', 'Opis Projektu', 'started', '2020-10-27 09:35:55', '2020-10-27 09:35:55'),
(34, 'Project', 'description project', 'started', '2020-10-27 09:36:21', '2020-10-27 09:36:21'),
(35, 'Jakiś projekt', 'Opis Projektu', 'started', '2020-10-27 09:52:30', '2020-10-27 09:52:30'),
(36, 'Jakiś projekt', 'Opis Projektu', NULL, '2020-10-27 09:53:17', '2020-10-27 09:53:17'),
(37, 'Jakiś projekt', 'Jakiś opis', NULL, '2020-10-27 09:56:47', '2020-10-27 09:56:47'),
(38, 'Jakiś projekt', 'Jakiś opis', 'started', '2020-10-27 10:52:55', '2020-10-27 10:52:55'),
(40, 'Jakiś projekt', 'Jakiś opis', 'draft', '2020-10-28 09:32:10', '2020-10-28 09:32:10');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rewards`
--

CREATE TABLE `rewards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `projectId` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `rewards`
--

INSERT INTO `rewards` (`id`, `projectId`, `name`, `description`, `amount`, `created_at`, `updated_at`) VALUES
(12, 40, 'Jakiś projekt', 'Jakiś opis', '12.22', '2020-10-28 10:20:44', '2020-10-28 10:20:44');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `rewards`
--
ALTER TABLE `rewards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rewards_projectid_foreign` (`projectId`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT dla tabeli `rewards`
--
ALTER TABLE `rewards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `rewards`
--
ALTER TABLE `rewards`
  ADD CONSTRAINT `rewards_projectid_foreign` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
